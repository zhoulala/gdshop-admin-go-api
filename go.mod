module gdshop-admin-go-api

go 1.16

require (
	github.com/SongLiangChen/RateLimiter v1.0.5
	github.com/afocus/captcha v0.0.0-20191010092841-4bd1f21c8868
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/getsentry/sentry-go v0.7.0
	github.com/gogf/gcache-adapter v0.1.2
	github.com/gogf/gf v1.16.1
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/gookit/event v1.0.5
	github.com/hprose/hprose-golang v2.0.6+incompatible
	github.com/json-iterator/go v1.1.11
	github.com/kr/text v0.2.0 // indirect
	github.com/mushroomsir/image-type v0.0.0-20170825072210-4e178dd6cd0e
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/prometheus/client_golang v1.10.0
	github.com/rwcarlsen/goexif v0.0.0-20190401172101-9e8deecbddbd
	github.com/shenghui0779/gochat v1.1.10
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	github.com/stretchr/testify v1.7.0
	github.com/syyongx/php2go v0.9.4
	github.com/tidwall/gjson v1.8.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/xxtea/xxtea-go v0.0.0-20170828040851-35c4b17eecf6
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
	golang.org/x/image v0.0.0-20210216034530-4410531fe030
	golang.org/x/text v0.3.5 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
