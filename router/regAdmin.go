package router

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func regAdmin(s *ghttp.Server, versionName, defaultVersionName string) {
	// 路由前缀
	routerPrefix := "/gdshop/admin"

	var leftJoin []*BaseReq.QueryOpLeftJoin
	dbPrefix := g.Cfg().GetString("database.prefix")
	leftJoin = append(leftJoin, &BaseReq.QueryOpLeftJoin{
		TableInfo: dbPrefix + "sys_department sd",
		Condition: "a.dept_id = sd.id",
	})

	c := controllers.NewAdminController(&BaseReq.I{
		TableName:          "admin",
		InfoIgnoreProperty: "password,password_salt,update_at,delete_at", // info 时过滤
		UpdateIgnoreProperty: []string{
			"account",
		},
		PageQueryOp: &BaseReq.QueryOp{
			SelectFields: []string{
				"a.id",
				"a.account",
				"a.dept_id",
				"a.realname",
				"a.nickname",
				"a.avatar",
				"a.phone",
				"a.remark",
				"a.status",
				"a.create_at",
				"sd.name AS departmentName",
			},
			AsName:   "a",
			LeftJoin: leftJoin,
			OtherWhere: func(r *ghttp.Request) g.Map {
				departmentIds := r.GetArray("departmentIds")
				data := g.Map{
					"a.account != ?": "admin",
				}
				if departmentIds != nil {
					data["a.dept_id IN (?)"] = departmentIds
				}
				roleIds := r.GetArray("roleIds")
				if roleIds != nil {
					data["a.id IN (SELECT admin_id FROM gd_admin_role WHERE role_id IN (?))"] = roleIds
				}
				return data
			},
		},
	})
	controllerMaps := map[string]interface{}{
		"1.0.0": c,
	}

	s.Group(routerPrefix, func(group *ghttp.RouterGroup) {
		// 登录判断
		group.Middleware(MiddlewareIsMemberLogin)

		group.Group("/", func(permGroup *ghttp.RouterGroup) {
			// 权限判断
			permGroup.Middleware(MiddlewareMemberPermissions)
			// 注册基础方法
			regBase([]string{
				"add",
				"delete",
				"update",
				"info",
				"list",
				"page",
			}, permGroup, controllerMaps, versionName, defaultVersionName)

			permGroup.POST("/move", func(r *ghttp.Request) {
				BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Move")
			})

			permGroup.POST("/reset_pass", func(r *ghttp.Request) {
				BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "ResetPass")
			})
		})
		// 不需要判断权限的
		group.Group("/", func(openGroup *ghttp.RouterGroup) {
			openGroup.GET("/person", func(r *ghttp.Request) {
				BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Person")
			})
			openGroup.GET("/permmenu", func(r *ghttp.Request) {
				BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Permmenu")
			})
			openGroup.POST("/personUpdate", func(r *ghttp.Request) {
				BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "PersonUpdate")
			})

			openGroup.POST("/change_pass", func(r *ghttp.Request) {
				BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "ChangePass")
			})
		})
	})
	// 不需要登录的
	s.Group(routerPrefix, func(group *ghttp.RouterGroup) {
		group.POST("/logout", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Logout")
		})
	})
}
