package router

import (
	"gdshop-admin-go-api/app/controllers/shop"
	"gdshop-admin-go-api/app/request/BaseReq"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
)

func regShop(s *ghttp.Server, versionName, defaultVersionName string) {
	// 路由前缀
	routerPrefix := "/shop"
	// 注册 店铺
	s.Group(routerPrefix+"/store", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewStoreController(&BaseReq.I{
			TableName:          "store",
			InfoIgnoreProperty: "delete_at", // info 时过滤
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)
	})

	// 注册 商品
	s.Group(routerPrefix+"/goods", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		var leftJoin []*BaseReq.QueryOpLeftJoin
		dbPrefix := g.Cfg().GetString("database.prefix")
		leftJoin = append(leftJoin, &BaseReq.QueryOpLeftJoin{
			TableInfo: dbPrefix + "store s",
			Condition: "s.id = g.store_id",
		})

		c := shop.NewGoodsController(&BaseReq.I{
			TableName:          "goods",
			InfoIgnoreProperty: "delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				KeyWordLikeFields: g.ArrayStr{
					"goods_name",
					"sub_title",
					"short_title",
				},
				SelectFields: g.ArrayStr{
					"g.*",
					"s.store_name",
				},
				AsName:   "g",
				LeftJoin: leftJoin,
				OrderBy:  "g.display_sort DESC,g.update_at DESC,g.id DESC",
				OtherWhere: func(r *ghttp.Request) g.Map {
					where := g.Map{}
					categoryId := r.GetInt("category_id", 0)
					if categoryId > 0 {
						where["cates LIKE ?"] = "%," + gconv.String(categoryId) + ",%"
					}
					return where
				},
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.GET("/get_form_price", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "GetFormPrice")
		})
		group.GET("/get_form_stock", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "GetFormStock")
		})

		group.POST("/set_form_price", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "SetFormPrice")
		})
		group.POST("/set_form_stock", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "SetFormStock")
		})

		group.POST("/set_status", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "SetStatus")
		})

		group.GET("/get_diy_goods_data", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "GetDiyGoodsData")
		})
	})

	// 注册 订单
	s.Group(routerPrefix+"/order", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		var leftJoin []*BaseReq.QueryOpLeftJoin
		dbPrefix := g.Cfg().GetString("database.prefix")
		leftJoin = append(leftJoin, &BaseReq.QueryOpLeftJoin{
			TableInfo: dbPrefix + "member m",
			Condition: "m.id = o.member_id",
		})
		leftJoin = append(leftJoin, &BaseReq.QueryOpLeftJoin{
			TableInfo: dbPrefix + "order_price op",
			Condition: "o.id = op.order_id",
		})
		leftJoin = append(leftJoin, &BaseReq.QueryOpLeftJoin{
			TableInfo: dbPrefix + "store s",
			Condition: "o.store_id = s.id",
		})

		c := shop.NewOrderController(&BaseReq.I{
			TableName:          "order",
			InfoIgnoreProperty: "delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				SelectFields: g.ArrayStr{
					"o.*",
					"m.realname AS buy_realname",
					"m.mobile AS buy_mobile",
					"op.dispatch_price_change",
					//"IFNULL(op.dispatch_price_change,0) AS dispatch_price_change",
					//"IFNULL(op.total_price_change,0) AS total_price_change",
					"op.total_price_change",
					"s.store_name",
				},
				AsName:   "o",
				LeftJoin: leftJoin,
				OrderBy:  "o.id DESC",
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 权限判断
		group.Middleware(MiddlewareMemberPermissions)
		// 注册基础方法
		regBase([]string{
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.POST("/change_remark", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "ChangeRemark")
		})

		group.POST("/set_price_change", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "SetPriceChange")
		})

		group.POST("/change_status", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "ChangeStatus")
		})

		group.GET("/get_address_info", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "GetAddressInfo")
		})

		group.GET("/get_express_info", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "GetExpressInfo")
		})

		group.GET("/get_express_info_list", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "GetExpressInfoList")
		})

		group.GET("/get_list_types", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "GetListTypes")
		})

		group.GET("/discount_info", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "DiscountInfo")
		})
	})

	// 注册 物流公司
	s.Group(routerPrefix+"/express", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewExpressController(&BaseReq.I{
			InfoIgnoreProperty: "delete_at", // info 时过滤
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 注册基础方法
		regBase([]string{
			"list",
		}, group, controllerMaps, versionName, defaultVersionName)
	})
	// 售后
	s.Group(routerPrefix+"/orderRefund", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewOrderRefundController(&BaseReq.I{
			InfoIgnoreProperty: "delete_at", // info 时过滤
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 注册基础方法
		regBase([]string{
			"info",
			"list",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.GET("/get_list_types", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "GetListTypes")
		})

		group.POST("/page", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Page")
		})

		group.POST("/submitHandle", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "SubmitHandle")
		})

		group.GET("/getHandle", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "GetHandle")
		})

		group.GET("/refund", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "Refund")
		})
	})

	// 商品限购
	s.Group(routerPrefix+"/goodsLimit", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewGoodsLimitController(&BaseReq.I{
			InfoIgnoreProperty: "delete_at", // info 时过滤
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.POST("/updateLimitRules", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "UpdateLimitRules")
		})

		group.GET("/getLimitRules", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "GetLimitRules")
		})
	})

	// 商城设置
	s.Group(routerPrefix+"/config", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewConfigController(&BaseReq.I{
			InfoIgnoreProperty: "delete_at", // info 时过滤
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 注册基础方法
		regBase([]string{
			"update",
			"info",
		}, group, controllerMaps, versionName, defaultVersionName)
	})
}
