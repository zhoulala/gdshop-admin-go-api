package router

import (
	"gdshop-admin-go-api/library/response"
	"gdshop-admin-go-api/library/tools/prometheusHelp"
	"github.com/SongLiangChen/RateLimiter"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/os/glog"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var logger *glog.Logger

// 你可以将路由注册放到一个文件中管理，
// 也可以按照模块拆分到不同的文件中管理，
// 但统一都放到router目录下。
func init() {
	versionName := g.Cfg().GetString("site.ApiVersionName")
	defaultVersionName := g.Cfg().GetString("site.ApiDefaultVersionName")
	logger = g.Log()
	s := g.Server()
	s.Use(MiddlewareErrorHandler)

	allRules := RateLimiter.NewRules()
	// 全站限流
	allRules.AddRule("all", &RateLimiter.Rule{
		Duration: 1,  // 秒
		Limit:    10, // 次
	})
	// 验证码 限流
	allRules.AddRule("/gdshop/open/captcha", &RateLimiter.Rule{
		Duration: 1, // 秒
		Limit:    2, // 次
	})
	// 登陆 限流
	allRules.AddRule("/gdshop/open/login", &RateLimiter.Rule{
		Duration: 1, // 秒
		Limit:    2, // 次
	})
	allRateLimiter, rlIsOk := RateLimiter.NewRateLimiter("memory")
	if rlIsOk == false {
		panic("限流器加载失败")
	}
	err := allRateLimiter.InitRules(allRules)
	if err != nil {
		panic(err)
	}

	s.Use(prometheusHelp.PromMiddleware(nil))
	s.BindHandler("/metrics", prometheusHelp.PromHandler(promhttp.Handler()))

	s.BindHandler("/", func(r *ghttp.Request) {
		r.Response.WriteExit("gdshop-admin API is OK")
	})

	// 限流器
	s.Use(func(r *ghttp.Request) {
		// 不限用户，全站限流
		if !allRateLimiter.TokenAccess("all", "all") {
			r.Response.ClearBuffer()
			r.Response.WriteJsonExit(response.FailByRequestRateLimiter(r))
			return
		}
		// 不限用户，根据URI 限流
		if !allRateLimiter.TokenAccess("all", r.Request.URL.Path) {
			r.Response.ClearBuffer()
			r.Response.WriteJsonExit(response.FailByRequestRateLimiter(r))
			return
		}

		r.Middleware.Next()
	})

	regAdmin(s, versionName, defaultVersionName)
	regHome(s, versionName, defaultVersionName)
	regMember(s, versionName, defaultVersionName)
	regCategory(s, versionName, defaultVersionName)

	regGdshop(s, versionName, defaultVersionName)
	regCms(s, versionName, defaultVersionName)
	//regArticle(s, versionName, defaultVersionName)
	//regSlide(s, versionName, defaultVersionName)
	//regLink(s, versionName, defaultVersionName)

	regDiy(s, versionName, defaultVersionName)
	regShop(s, versionName, defaultVersionName)
	regActivity(s, versionName, defaultVersionName)
	// 分销
	regFenxiao(s, versionName, defaultVersionName)

	// 取消队列路由注册
	//regQueue(s, versionName, defaultVersionName)

	regPayNotify(s, versionName, defaultVersionName)

	initRpcServer()
}
