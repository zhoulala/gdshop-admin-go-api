package router

import (
	"gdshop-admin-go-api/app/controllers/shop"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
)

func regDiy(s *ghttp.Server, versionName, defaultVersionName string) {
	// 路由前缀
	routerPrefix := "/shop"

	s.Group(routerPrefix+"/diyPage", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewDiyPageController(&BaseReq.I{
			TableName:          "diy_page",
			InfoIgnoreProperty: "delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				SelectFields: []string{
					"id",
					"page_name",
					"page_type",
					"is_activated",
					"status",
					"create_at",
					"update_at",
				},
				FieldsEq: []string{
					"page_type",
				},
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"list",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)

		group.POST("/set_activated", func(r *ghttp.Request) {
			BindHandlerVersion(r, versionName, controllerMaps, defaultVersionName, "SetActivated")
		})
	})

	s.Group(routerPrefix+"/diyPageItem", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewDiyPageItemController(&BaseReq.I{
			TableName:          "diy_page_item",
			InfoIgnoreProperty: "delete_at", // info 时过滤
			PageQueryOp: &BaseReq.QueryOp{
				OrderBy: "sort DESC",
				SelectFields: []string{
					"id",
					"type_id",
					"page_id",
					"title",
					"sort",
					"status",
					"create_at",
					"update_at",
				},
				FieldsEq: []string{
					"page_id",
				},
			},
			AddBeforeFn: func(r *ghttp.Request, data map[string]interface{}) *response.JsonResponse {
				data["json_data"] = gconv.String(data["json_data"])
				return response.SuccessByRequestMessage(r, "")
			},
			UpdateBeforeFn: func(r *ghttp.Request, data map[string]interface{}) *response.JsonResponse {
				data["json_data"] = gconv.String(data["json_data"])
				return response.SuccessByRequestMessage(r, "")
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 注册基础方法
		regBase([]string{
			"add",
			"delete",
			"update",
			"info",
			"page",
		}, group, controllerMaps, versionName, defaultVersionName)
	})

	s.Group(routerPrefix+"/diyPageType", func(group *ghttp.RouterGroup) {
		group.Middleware(MiddlewareIsMemberLogin)

		c := shop.NewDiyPageTypeController(&BaseReq.I{
			TableName:          "diy_page_type",
			InfoIgnoreProperty: "delete_at", // info 时过滤
			ListQueryOp: &BaseReq.QueryOp{
				SelectFields: []string{
					"id",
					"type_name",
					"type_code",
					"parent_id",
				},
				OtherWhere: func(r *ghttp.Request) g.Map {
					tmp := r.GetInt("array", 0)
					fanhui := g.Map{}
					if tmp == 1 {
						fanhui["parent_id > ?"] = 0
					}

					return fanhui
				},
			},
		})
		controllerMaps := map[string]interface{}{
			"1.0.0": c,
		}
		// 注册基础方法
		regBase([]string{
			"list",
		}, group, controllerMaps, versionName, defaultVersionName)
	})
}
