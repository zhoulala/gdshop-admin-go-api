## gdshop-admin-go

### 感谢

1. 后端基于 [goframe 1.16.x](https://goframe.org/display/gf)

### 前端项目

[https://gitee.com/hackchen/gdshop-admin-scui](https://gitee.com/hackchen/gdshop-admin-scui)

### 环境

1. MySQL
1. Redis

### 目录说明
![目录](https://images.gitee.com/uploads/images/2021/0506/193325_b8dd885a_331823.png "目录")

### 安装

1. 导入res/db 下的 gdshop.sql 文件
1. 安装依赖包
1. 配置 config/config.toml 文件里面的MySQL和Redis的连接信息
1. 配置队列组件（根目录下的delayer目录）
1. 根据相应的系统启动对应的队列组件（根目录下的delayer目录）
1. 启动项目

### 声明
1. 本软件遵循《木兰宽松许可证》开源协议，意味着您无需支付任何费用，也无需授权，即可将它应用到您的产品中。
1. 本软件禁止使用在非法领域，比如涉及赌博，暴力等方面。如因此产生纠纷等法律问题，本软件不承担任何责任。

### 交流QQ群：822176616
![交流QQ群：822176616](http://kuke365-img.winlo.net/project_demo/gdshop/gdshop_qun.png "交流群")