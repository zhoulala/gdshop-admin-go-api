// ==========================================================================
// This is auto-generated by gf cli tool. You may not really want to edit it.
// ==========================================================================

package entity

// Goods is the golang structure for table gd_goods.
type Goods struct {
	Id             uint   `orm:"id,primary"       json:"id"`               //
	StoreId        int    `orm:"store_id"         json:"store_id"`         // 所属店铺 0为自营
	GoodsName      string `orm:"goods_name"       json:"goods_name"`       // 商品名称
	Unit           string `orm:"unit"             json:"unit"`             // 商品单位：如个、件、包
	SubTitle       string `orm:"sub_title"        json:"sub_title"`        // 副标题
	ShortTitle     string `orm:"short_title"      json:"short_title"`      // 商品短标题 商品短标题 用于快递打印,以及小型热敏打印机打印
	Type           int    `orm:"type"             json:"type"`             // 商品类型 1实体商品 2虚拟商品 3虚拟物品(卡密)
	Cates          string `orm:"cates"            json:"cates"`            // 分类 可多选，例如 ,1,2,
	DisplaySort    int    `orm:"display_sort"     json:"display_sort"`     // 排序 数字越大，排名越靠前,如果为空，默认排序方式为创建时间
	Thumbs         string `orm:"thumbs"           json:"thumbs"`           // 产品图，第一张为封面图，建议为正方型图片，其他为详情页面图片，尺寸建议宽度为640，并保持图片大小一致
	VideoUrl       string `orm:"video_url"        json:"video_url"`        // 视频地址
	Sales          int    `orm:"sales"            json:"sales"`            // 销量
	VirtualSales   int    `orm:"virtual_sales"    json:"virtual_sales"`    // 虚拟销量
	IsShowSales    int    `orm:"is_show_sales"    json:"is_show_sales"`    // 显示销量
	IsShowStock    int    `orm:"is_show_stock"    json:"is_show_stock"`    // 显示库存
	MinPrice       int64  `orm:"min_price"        json:"min_price"`        // 最低价格 单位分
	MaxPrice       int64  `orm:"max_price"        json:"max_price"`        // 最高价格 单位分
	Stock          int    `orm:"stock"            json:"stock"`            // 库存 算出在规格里面总的库存，写入这里
	StockCnf       int    `orm:"stock_cnf"        json:"stock_cnf"`        // 库存策略 0拍下减库存 1付款减库存 2永不减库存
	IsRecommand    int    `orm:"is_recommand"     json:"is_recommand"`     // 商品属性 推荐 1是
	IsNew          int    `orm:"is_new"           json:"is_new"`           // 商品属性 新品 1是
	IsHot          int    `orm:"is_hot"           json:"is_hot"`           // 商品属性 热销 1是
	Status         int    `orm:"status"           json:"status"`           // 状态 1上架 0否
	IsAdminDisable int    `orm:"is_admin_disable" json:"is_admin_disable"` // 管理员禁用 0正常
	Spu            string `orm:"spu"              json:"spu"`              // 商品编码
	CreateAt       uint   `orm:"create_at"        json:"create_at"`        // 创建时间
	UpdateAt       uint   `orm:"update_at"        json:"update_at"`        // 更新时间
	DeleteAt       uint   `orm:"delete_at"        json:"delete_at"`        // 删除时间 0为正常
}
