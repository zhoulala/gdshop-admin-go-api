package entity

type PayLog struct {
	Id            int    `orm:"id,primary"     json:"id"`             //
	ProjectId     int    `orm:"project_id"     json:"project_id"`     // 所属项目
	Appid         string `orm:"appid"          json:"appid"`          // appid
	MchId         string `orm:"mch_id"         json:"mch_id"`         // 商户号
	OrderId       string `orm:"order_id"       json:"order_id"`       // 所属订单 订单号经过编码
	PaymentMethod int    `orm:"payment_method" json:"payment_method"` // 支付方式 1微信 2支付宝
	PayType       int    `orm:"pay_type"       json:"pay_type"`       // 支付类型 0 全部款项（按付款折扣） 1定金（无折扣）
	OpenId        string `orm:"open_id"        json:"open_id"`        //
	Money         int    `orm:"money"          json:"money"`          // 付款的钱数 单位分
	CreateTime    int64  `orm:"create_time"    json:"create_time"`    // 创建时间 毫秒
	UpdateTime    int64  `orm:"update_time"    json:"update_time"`    // 毫秒
	Status        int    `orm:"status"         json:"status"`         // 状态 0发起支付 1支付成功 2支付失败 3找不到发起数据 4修改订单 已付 失败
	Remark        string `orm:"remark"         json:"remark"`         // 失败备注
}
