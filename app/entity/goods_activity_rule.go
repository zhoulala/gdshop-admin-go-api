package entity

// GoodsActivityRule is the golang structure for table gd_goods_activity_rule.
type GoodsActivityRule struct {
	ActivityId int    `orm:"activity_id,primary" json:"activity_id"` // 活动ID
	Rules      string `orm:"rules" json:"rules"`                     // 活动规则
	Loop       int    `orm:"loop" json:"loop"`
}
