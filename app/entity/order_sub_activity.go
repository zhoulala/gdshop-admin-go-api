// ==========================================================================
// This is auto-generated by gf cli tool. You may not really want to edit it.
// ==========================================================================

package entity

// OrderSubActivity is the golang structure for table gd_order_sub_coupon.
type OrderSubActivity struct {
	Id         int   `orm:"id,primary"       json:"id"`         //
	OrderId    int   `orm:"order_id"         json:"orderId"`    // 所属ID
	OrderSubId int   `orm:"order_sub_id"     json:"orderSubId"` // 所属子订单ID
	ActivityId int   `orm:"activity_id" json:"activityId"`      // 活动ID
	Paid       int   `orm:"paid"        json:"paid"`            // 实付
	Discount   int64 `orm:"discount"         json:"discount"`   // 本次优惠金额 单位分

	ActivityName string `orm:"activity_name" json:"activityName"` // 活动名称
}
