package entity

type MemberWechat struct {
	Id           uint   `orm:"id,primary"   json:"id"`           //
	ProjectId    int    `orm:"project_id"   json:"projectId"`    // 所属项目
	MemberId     int    `orm:"member_id"    json:"memberId"`     // 所属会员
	Openid       string `orm:"openid"       json:"openid"`       // 微信openid
	Nickname     string `orm:"nickname"     json:"nickname"`     // 微信用户信息 昵称
	Gender       int    `orm:"gender"       json:"gender"`       // 微信用户信息 性别
	Avatar       string `orm:"avatar"       json:"avatar"`       // 微信用户信息 头像
	Language     string `orm:"language"     json:"language"`     // 微信用户信息 语言
	Country      string `orm:"country"      json:"country"`      // 微信用户信息 国家
	Province     string `orm:"province"     json:"province"`     // 微信用户信息 省份
	City         string `orm:"city"         json:"city"`         // 微信用户信息 城市
	IsFollow     uint   `orm:"is_follow"    json:"isFollow"`     // 是否关注 1是
	Followtime   uint   `orm:"followtime"   json:"followtime"`   // 关注时间
	Unfollowtime uint   `orm:"unfollowtime" json:"unfollowtime"` // 取消关注时间
	Remark       string `orm:"remark"       json:"remark"`       // 备注
}
