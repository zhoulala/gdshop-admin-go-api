package entity

// GoodsActivitySeckill is the golang structure for table gd_goods_activity_seckill.
type GoodsActivitySeckill struct {
	ActivityId    int   `orm:"activity_id,primary" json:"activity_id"`     // 活动ID
	GoodsId       int   `orm:"goods_id"            json:"goods_id"`        // 商品ID
	GoodsOptionId int   `orm:"goods_option_id"     json:"goods_option_id"` // 规格ID 对应类型3
	SeckillPrice  int64 `orm:"seckill_price"       json:"seckill_price"`   // 秒杀价 对应类型3
	Stock         int   `orm:"stock"               json:"stock"`           // 库存秒杀件数，0为根据规格库存
}
