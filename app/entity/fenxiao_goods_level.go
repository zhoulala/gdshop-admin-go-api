package entity

// FenxiaoGoodsLevel is the golang structure for table fenxiao_goods_level.
type FenxiaoGoodsLevel struct {
	Id             int  `orm:"id,primary"       json:"id"`               //
	FenxiaoGoodsId int  `orm:"fenxiao_goods_id" json:"fenxiao_goods_id"` //
	GoodsId        int  `orm:"goods_id"         json:"goods_id"`         //
	GoodsOptionId  int  `orm:"goods_option_id"  json:"goods_option_id"`  //
	LevelId        int  `orm:"level_id"         json:"level_id"`         // 分销等级ID
	OneRate        int  `orm:"one_rate"         json:"one_rate"`         // 店内(自购)佣金比例
	TwoRate        int  `orm:"two_rate"         json:"two_rate"`         // 上级佣金比例
	ThreeRate      int  `orm:"three_rate"       json:"three_rate"`       // 上上级佣金比例
	CreateAt       uint `orm:"create_at"        json:"create_at"`        // 创建时间
	UpdateAt       uint `orm:"update_at"        json:"update_at"`        // 更新时间
	DeleteAt       uint `orm:"delete_at"        json:"delete_at"`        // 删除时间 0为正常
}
