// ==========================================================================
// This is auto-generated by gf cli tool. You may not really want to edit it.
// ==========================================================================

package entity

// OrderExpress is the golang structure for table gd_order_express.
type OrderExpress struct {
	Id         uint   `orm:"id,primary"   json:"id"`           //
	OrderId    int    `orm:"order_id"     json:"order_id"`     // 所属订单
	OrderSubId int    `orm:"order_sub_id" json:"order_sub_id"` // 所属子订单
	ExpressCom string `orm:"express_com"  json:"express_com"`  // 选择的快递公司名称
	Express    string `orm:"express"      json:"express"`      // 快递公司代号
	ExpressSn  string `orm:"express_sn"   json:"express_sn"`   // 运单号
	CreateAt   int64  `orm:"create_at"    json:"create_at"`    // 创建时间
	UpdateAt   int64  `orm:"update_at"    json:"update_at"`    // 更新时间
	DeleteAt   int64  `orm:"delete_at"    json:"-"`            // 删除时间 0为正常
}
