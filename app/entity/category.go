// ==========================================================================
// This is auto-generated by gf cli tool. You may not really want to edit it.
// ==========================================================================

package entity

// Category is the golang structure for table gd_category.
type Category struct {
	I
	ProjectId int    `orm:"project_id" json:"project_id"` // 所属项目
	Type      uint   `orm:"type"       json:"type"`       // 类别 1文章；2幻灯片；3单页；4友情链接；5商品
	ParentId  int    `orm:"parent_id"  json:"parent_id"`  // 所属上级
	Ancestors string `orm:"ancestors"  json:"ancestors"`  // 祖级列表
	CatName   string `orm:"cat_name"   json:"cat_name"`   // 分类名称
	Letter    string `orm:"letter"     json:"letter"`     // 分类唯一标识
	Cover     string `orm:"cover"      json:"cover"`      // 封面URL
	JumpUrl   string `orm:"jump_url"   json:"jump_url"`   // 跳转URL
	Sort      int    `orm:"sort"       json:"sort"`       // 排序，大到小
}
