package entity

type I struct {
	Id       int `orm:"id,primary" json:"id"`
	CreateAt int `orm:"create_at"  json:"create_at"` // 创建时间
	UpdateAt int `orm:"update_at"  json:"update_at"` // 更新时间
	DeleteAt int `orm:"delete_at"  json:"delete_at"` // 删除时间 0为正常
}
