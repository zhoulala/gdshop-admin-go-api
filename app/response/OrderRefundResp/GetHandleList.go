package OrderRefundResp

type GetHandleList struct {
	Id                  int    `orm:"id,primary"             json:"id"`                     //
	OrderId             int    `orm:"order_id"               json:"order_id"`               // 所属订单
	OrderSubId          int    `orm:"order_sub_id"           json:"order_sub_id"`           // 所属子订单
	RefundNo            string `orm:"refund_no"              json:"refund_no"`              // 退款单号
	RefundType          int    `orm:"refund_type"            json:"refund_type"`            // 售后类型 1仅退款；2退货退款；3换货
	ReasonType          int    `orm:"reason_type"            json:"reason_type"`            // 退款原因 选项
	ApplyRemark         string `orm:"apply_remark"           json:"apply_remark"`           // 补充说明
	ApplyTime           int    `orm:"apply_time"             json:"apply_time"`             // 申请时间
	ApplyCertificate    string `orm:"apply_certificate"      json:"apply_certificate"`      // 补充凭证 一般指的是图片或者视频，保存链接
	ApplyMoney          int64  `orm:"apply_money"            json:"apply_money"`            // 申请退款金额 分
	Status              int    `orm:"status"                 json:"status"`                 // 售后状态 1申请中 2驳回 3同意
	ExpressCom          string `orm:"express_com"     json:"express_com"`                   // 选择的快递公司名称
	Express             string `orm:"express"         json:"express"`                       // 快递公司代号
	ExpressSn           string `orm:"express_sn"      json:"express_sn"`                    // 运单号
	HandleRemark        string `orm:"handle_remark"          json:"handle_remark"`          // 处理说明
	HandleTime          int    `orm:"handle_time"            json:"handle_time"`            // 处理时间
	HandleAdminId       int    `orm:"handle_admin_id"        json:"handle_admin_id"`        // 处理的管理员ID
	HandleStoreMemberId int    `orm:"handle_store_member_id" json:"handle_store_member_id"` // 处理的店铺人员ID
	HandleAdminName     string `json:"handle_admin_name"`
}
