package OrderResp

type PayLogs struct {
	CreateAt          int    `json:"create_at"`
	Money             int    `json:"money"`
	PaymentMethod     int    `json:"payment_method"`
	PaymentMethodName string `json:"payment_method_name"`
	OrderId           int    `json:"order_id"`
}
