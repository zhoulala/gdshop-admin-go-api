package OrderResp

import "gdshop-admin-go-api/app/entity"

type DiscountInfo struct {
	entity.OrderSub
	Activitys []*entity.OrderSubActivity `json:"activitys"`
	Coupons   []*entity.OrderSubCoupon   `json:"coupons"`
}
