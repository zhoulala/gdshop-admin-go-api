package OpenResp

type Login struct {
	Id       uint   `orm:"id,primary"    json:"id"`       //
	Account  string `orm:"account"        json:"account"` // 账号
	DeptId   int    `orm:"dept_id" json:"dept_id"`
	Realname string `orm:"realname"      json:"realname"` // 姓名
	Nickname string `orm:"nickname"      json:"nickname"` // 姓名

	Expire        int    `json:"expire"`
	RefreshExpire int    `json:"refreshExpire"`
	RefreshToken  string `json:"refreshToken"`
	Token         string `orm:"token"     json:"token"` // token

	UserInfo *LoginUserInfo `json:"userInfo"`
}

type LoginUserInfo struct {
	UserId       string   `json:"userId"`
	UserName     string   `json:"userName"`
	Avatar       string   `json:"avatar"`
	AvatarPrefix string   `json:"avatarPrefix"`
	Dashboard    string   `json:"dashboard"`
	Role         []string `json:"role"`
}
