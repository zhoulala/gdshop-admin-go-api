package HomeResp

type Login struct {
	Id           uint   `orm:"id,primary"    json:"id"`            //
	Mobile       string `orm:"mobile"        json:"mobile"`        // 手机号码
	Realname     string `orm:"realname"      json:"realname"`      // 姓名
	Nickname     string `orm:"nickname"      json:"nickname"`      // 姓名
	Status       int    `orm:"status"        json:"status"`        // 状态
	IntegralCoin int64  `orm:"integral_coin" json:"integral_coin"` // 积分
	MoneyCoin    uint64 `orm:"money_coin"    json:"money_coin"`    // 余额 单位分
	Remark       string `orm:"remark"        json:"remark"`        // 备注
	CreateAt     uint   `orm:"create_at"     json:"create_at"`     // 创建时间
	UpdateAt     uint   `orm:"update_at"     json:"update_at"`     // 更新时间
	Token        string `orm:"token"     json:"token"`             // token
}
