package GoodsResp

type SpecLeaf struct {
	Id     uint   `orm:"id"   json:"id"`              //
	SpecId int    `orm:"spec_id"      json:"spec_id"` // 所属规格ID
	Value  string `orm:"value"        json:"value"`   // 项标题
	Thumb  string `orm:"thumb"        json:"thumb"`   // 项对应的图片地址
}
