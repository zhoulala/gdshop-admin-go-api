package OrderReq

import "gdshop-admin-go-api/app/request/BaseReq"

type MemberCloseOrder struct {
	BaseReq.AdminLogin
	OrderId  int    `json:"order_id"`
	MemberId int    `json:"member_id"`
	Remark   string `json:"remark"`
}
