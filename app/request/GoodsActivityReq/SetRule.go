package GoodsActivityReq

import (
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/encoding/gjson"
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/util/gconv"
	"reflect"
)

type SetRule struct {
	ActivityId int         `json:"activity_id"`
	Loop       int         `json:"loop"`
	Rules      []RulesItem `json:"rules"`
}

type RulesItem struct {
	ThresholdNum string `json:"threshold_num"`
	DiscountNum  string `json:"discount_num"`
}

func (sr *SetRule) UnmarshalValue(value interface{}) error {
	if record, ok := value.(gdb.Map); ok {
		json, err := gjson.LoadContent(record["rules"])
		if err != nil {
			return err
		}
		var tmp []RulesItem
		err = json.Structs(&tmp)
		if err != nil {
			return gerror.Newf("rules 转换失败:%s", err)
		}
		*sr = SetRule{
			ActivityId: gconv.Int(record["activity_id"]),
			Loop:       gconv.Int(record["loop"]),
			Rules:      tmp,
		}
		return nil
	}
	return gerror.Newf(`unsupported value type for UnmarshalValue: %v`, reflect.TypeOf(value))
}
