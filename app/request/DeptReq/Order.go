package DeptReq

import "gdshop-admin-go-api/app/request/BaseReq"

type Order struct {
	BaseReq.AdminLogin
	Data []*OrderItem
}

type OrderItem struct {
	Id       int
	ParentId int
	Sort     int
}
