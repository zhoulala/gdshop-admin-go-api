package GoodsReq

// Update 修改商品
type Update struct {
	Id            int
	CategoryId    int
	CheckAttrs    []string
	Content       string
	Cover         string
	DisplaySort   int
	GoodsName     string
	ShortTitle    string
	SpecTables    []*SpecTableItem
	Attributes    []*GoodsAttributes
	Specs         []*Specs
	Spu           string
	SubTitle      string
	Thumbs        []string
	Unit          string
	VideoUrl      string
	VirtualSales  int
	MobileContent []*MobileContentItem
}
type SpecTableItem struct {
	CostPrice   float64
	GoodsSku    string `orm:"goods_sn"`
	IsOpen      bool   `orm:"-"`
	IsOpenInt   int    `orm:"is_open"`
	MarketPrice float64
	SellPrice   float64
	Specs       []string `orm:"-"`
	SpecsText   string   `orm:"specs"`
	Stock       int
	Thumb       string
	Title       string
	CreateAt    int64
}
type Specs struct {
	Id    string
	Value string
	Items []*SpecsItem
}
type SpecsItem struct {
	Id    string
	Value string
	Thumb string
}

type MobileContentItem struct {
	InputType   string `json:"inputType"`
	Color       string `json:"color"`
	Bcolor      string `json:"bcolor"`
	InputValue  string `json:"inputValue"`
	InputValues string `json:"inputValues"`
}

type GoodsAttributes struct {
	IconName string `json:"icon_name"`
	IconText string `json:"icon_text"`
}
