package BaseReq

import (
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

type I struct {
	TableName            string
	DbLinkName           string   // 使用数据库连接名称，如果不写，默认是default
	Id                   string   // info 时使用
	Ids                  []string // delete 时使用
	InfoIgnoreProperty   string   // info接口忽略返回的参数，如用户信息不想返回密码
	UpdateIgnoreProperty []string // Update接口忽略返回的参数
	PageQueryOp          *QueryOp // 分页 查询参数
	ListQueryOp          *QueryOp //	LIST 查询参数
	PageInfo                      // 分页信息
	AdminLogin
	AddReq    interface{} // 添加请求（用于验证），可为空
	UpdateReq interface{} // 更新请求（用于验证），可为空
	// 新增时可额外插入其他数据（默认数据，别名等可以放在这里处理），覆盖原数据
	AddInsertData func(r *ghttp.Request) g.Map
	// 编辑时可额外插入其他数据（默认数据，别名等可以放在这里处理），覆盖原数据
	UpdateInsertData func(r *ghttp.Request) g.Map
	AddBeforeFn      func(r *ghttp.Request, data map[string]interface{}) *response.JsonResponse                                // 添加前执行的方法（不建议使用，不在同一个事务里）
	AddAfterFn       func(r *ghttp.Request, data map[string]interface{}, result map[string]interface{}) *response.JsonResponse // 添加后执行的方法（不建议使用，不在同一个事务里）
	UpdateBeforeFn   func(r *ghttp.Request, data map[string]interface{}) *response.JsonResponse                                // 编辑前执行的方法（不建议使用，不在同一个事务里）
	UpdateAfterFn    func(r *ghttp.Request, data map[string]interface{}, result map[string]interface{}) *response.JsonResponse // 编辑后执行的方法（不建议使用，不在同一个事务里）
	DeleteBeforeFn   func(r *ghttp.Request, data map[string]interface{}) *response.JsonResponse                                // 删除前执行的方法（不建议使用，不在同一个事务里）
	DeleteAfterFn    func(r *ghttp.Request, data map[string]interface{}, result map[string]interface{}) *response.JsonResponse // 删除后执行的方法（不建议使用，不在同一个事务里）
	InfoBeforeFn     func(r *ghttp.Request) *response.JsonResponse                                                             // info前执行的方法（不建议使用，不在同一个事务里）
	InfoAfterFn      func(r *ghttp.Request, result map[string]interface{}) (map[string]interface{}, error)                     // info后执行的方法（不建议使用，不在同一个事务里）
}

type QueryOp struct {
	KeyWordLikeFields   []string                     // 模糊查询字段 优先级 2
	FieldsEq            []string                     // 等于字段	优先级 1
	SelectFields        []string                     // 显示的字段
	AsName              string                       // 主表别名
	LeftJoin            []*QueryOpLeftJoin           // 关联表查询
	CloseDeleteAtSelect bool                         // 是否关闭 delete_at 查询
	OtherWhere          func(r *ghttp.Request) g.Map // 其他条件方法 优先级 3
	OrderBy             string                       // 排序
	IsCache             int                          // 开启缓存，秒
}

type QueryOpLeftJoin struct {
	TableInfo string // 左连接表信息
	Condition string // 左连接条件
}
