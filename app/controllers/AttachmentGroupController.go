package controllers

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/AttachmentGroupService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

type AttachmentGroupController struct {
	*BaseController
}

func NewAttachmentGroupController(inputReq *BaseReq.I) *AttachmentGroupController {
	return &AttachmentGroupController{
		&BaseController{Req: inputReq},
	}
}

func (c *AttachmentGroupController) List(r *ghttp.Request) *response.JsonResponse {
	return AttachmentGroupService.List(r, c.Req)
}
