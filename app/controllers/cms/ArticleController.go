package cms

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/ArticleService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

type ArticleController struct {
	*CmsBaseController
}

func NewArticleController(inputReq *BaseReq.I) *ArticleController {
	return &ArticleController{
		&CmsBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *ArticleController) Add(r *ghttp.Request) *response.JsonResponse {
	if c.Req.AddReq != nil {
		if err := r.Parse(c.Req.AddReq); err != nil {
			return response.FailByRequestMessage(r, err.Error())
		}
	}
	return ArticleService.Add(r, c.Req)
}

func (c *ArticleController) Update(r *ghttp.Request) *response.JsonResponse {
	if c.Req.UpdateReq != nil {
		if err := r.Parse(c.Req.UpdateReq); err != nil {
			return response.FailByRequestMessage(r, err.Error())
		}
	}
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return ArticleService.Update(r, c.Req)
}

func (c *ArticleController) Info(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return ArticleService.Info(r.GetCtx(), c.Req)
}
