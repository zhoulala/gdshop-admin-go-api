package cms

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/service/BaseService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gvalid"
)

type CmsBaseController struct {
	*controllers.BaseController
}

func (c *CmsBaseController) checkId(r *ghttp.Request) error {
	c.Req.Id = r.GetString("id", "")
	if err := gvalid.CheckValue(r.GetCtx(), c.Req.Id, "required", "ID不能为空"); err != nil {
		return err
	}

	return nil
}
func (c *CmsBaseController) checkIds(r *ghttp.Request) error {
	//c.Req.Id = r.GetString("ids", "")
	c.Req.Ids = r.GetArray("ids")
	//fmt.Println(c.Req.Ids)
	//if err := gvalid.Check(c.Req.Id, "required", "IDS不能为空"); err != nil {
	//	return err
	//}
	//c.Req.Ids = strings.Split(strings.Trim(c.Req.Id, ","), ",")

	return nil
}
func (c *CmsBaseController) Move(r *ghttp.Request) *response.JsonResponse {
	return BaseService.CategoryMove(r, c.Req.TableName, r.GetInt("category_id"), r.GetArray("ids"),
		"category_id")
}
