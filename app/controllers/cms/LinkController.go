package cms

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
)

type LinkController struct {
	*CmsBaseController
}

func NewLinkController(inputReq *BaseReq.I) *LinkController {
	return &LinkController{
		&CmsBaseController{&controllers.BaseController{Req: inputReq}},
	}
}
