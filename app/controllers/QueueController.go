package controllers

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/request/OrderReq"
	"gdshop-admin-go-api/app/service/OrderService"
	"gdshop-admin-go-api/library/response"
	"gdshop-admin-go-api/library/tools/OrderQueue"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

type QueueController struct {
	*BaseController
}

func NewQueueController(inputReq *BaseReq.I) *QueueController {
	return &QueueController{
		&BaseController{Req: inputReq},
	}
}

func (c *QueueController) MemberPay(r *ghttp.Request) *response.JsonResponse {
	hash := r.GetString("hash", "")
	if hash == "" {
		return response.FailByRequestMessage(r, "hash 不能为空")
	}
	doVar, err := g.Redis().DoVar("GET", "SubmitPay:"+hash)
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	if doVar.IsEmpty() {
		return response.FailByRequestMessage(r, "失败")
	}
	var params *OrderReq.MemberPay
	err = doVar.Struct(&params)
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return OrderService.MemberPay(r.GetCtx(), params)
}

func (c *QueueController) PostMemberPay(r *ghttp.Request) *response.JsonResponse {
	var params *OrderReq.MemberPay
	if err := r.Parse(&params); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return OrderService.MemberPay(r.GetCtx(), params)
}

/*func (c *QueueController) AddQueueCloseOrder(r *ghttp.Request) *response.JsonResponse {
	hash := r.GetString("hash", "")
	if hash == "" {
		return response.FailByRequestMessage(r, "hash 不能为空")
	}
	doVar, err := g.Redis().DoVar("GET", "CloseNoPayOrder:"+hash)
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	if doVar.IsEmpty() {
		return response.FailByRequestMessage(r, "失败")
	}
	orderId := doVar.Int()
	if orderId < 1 {
		return response.FailByRequestMessage(r, "order_id 不能为空")
	}
	_, err = OrderQueue.AddCloseNoPayOrder(orderId)
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return response.SuccessByRequestMessage(r, "成功")
}*/

/*func (c *QueueController) AddQueue(r *ghttp.Request) *response.JsonResponse {
	orderId := r.GetInt("order_id", 0)
	if orderId < 1 {
		return response.FailByRequestMessage(r, "order_id 不能为空")
	}
	OrderQueue.AddCloseNoPayOrder(orderId)
	return response.SuccessByRequestMessage(r, "成功")
}*/

func (c *QueueController) MemberCloseOrder(r *ghttp.Request) *response.JsonResponse {
	hash := r.GetString("hash", "")
	if hash == "" {
		return response.FailByRequestMessage(r, "hash 不能为空")
	}
	doVar, err := g.Redis().DoVar("GET", "MemberCloseOrder:"+hash)
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	if doVar.IsEmpty() {
		return response.FailByRequestMessage(r, "失败")
	}
	var tmp OrderReq.MemberCloseOrder
	err = doVar.Struct(&tmp)
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	jieguo := OrderService.MemberCloseOrder(r.GetCtx(), &tmp)

	if jieguo.Code == 0 {
		OrderService.GiveBackCouponByOrderId(r.GetCtx(), tmp.OrderId)
	}

	return jieguo
}

func (c *QueueController) AddCompleteOrder(r *ghttp.Request) *response.JsonResponse {
	orderId := r.GetInt("order_id", 0)
	if orderId < 1 {
		return response.FailByRequestMessage(r, "order_id 不能为空")
	}
	messageId, err := OrderQueue.AddCompleteOrder(orderId)
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return response.SuccessByRequestMessageData(r, "成功", g.Map{
		"messageId": messageId,
	})
}
