package activity

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/GoodsActivityService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

// GoodsActivityController 活动
type GoodsActivityController struct {
	*ActivityBaseController
}

func NewGoodsActivityController(inputReq *BaseReq.I) *GoodsActivityController {
	return &GoodsActivityController{
		&ActivityBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *GoodsActivityController) SetStatus(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkIds(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return GoodsActivityService.SetStatus(r, c.Req)
}

func (c *GoodsActivityController) GetRule(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkIds(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return GoodsActivityService.GetRule(r, c.Req)
}

func (c *GoodsActivityController) SetRule(r *ghttp.Request) *response.JsonResponse {
	return GoodsActivityService.SetRule(r, c.Req)
}

func (c *GoodsActivityController) GetExcludeGoodsSelects(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkIds(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return GoodsActivityService.GetExcludeGoodsSelects(r, c.Req)
}
