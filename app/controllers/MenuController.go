package controllers

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/MenuService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

type MenuController struct {
	*BaseController
}

func NewMenuController(inputReq *BaseReq.I) *MenuController {
	return &MenuController{
		&BaseController{Req: inputReq},
	}
}

func (c *MenuController) Delete(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkIds(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return MenuService.Delete(r, c.Req)
}
