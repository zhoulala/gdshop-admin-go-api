package controllers

import (
	"gdshop-admin-go-api/app/request/BaseReq"
)

type ConfigsController struct {
	*BaseController
}

func NewConfigsController(inputReq *BaseReq.I) *ConfigsController {
	return &ConfigsController{
		&BaseController{Req: inputReq},
	}
}
