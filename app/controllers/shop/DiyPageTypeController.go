package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
)

// DiyPageTypeController 装修页面类型
type DiyPageTypeController struct {
	*ShopBaseController
}

func NewDiyPageTypeController(inputReq *BaseReq.I) *DiyPageTypeController {
	return &DiyPageTypeController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}
