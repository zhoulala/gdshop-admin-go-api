package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/ShopConfigService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

type ConfigController struct {
	*ShopBaseController
}

func NewConfigController(inputReq *BaseReq.I) *ConfigController {
	inputReq.TableName = "config"
	return &ConfigController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *ConfigController) Info(r *ghttp.Request) *response.JsonResponse {
	return ShopConfigService.Info(r.GetCtx(), c.Req)
}

func (c *ConfigController) Update(r *ghttp.Request) *response.JsonResponse {
	return ShopConfigService.Update(r, c.Req)
}
