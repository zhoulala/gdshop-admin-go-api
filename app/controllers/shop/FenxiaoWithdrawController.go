package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/FenxiaoWithdrawService"
	"gdshop-admin-go-api/library/response"
	"gdshop-admin-go-api/library/tools/token"
	"github.com/gogf/gf/net/ghttp"
)

// FenxiaoWithdrawController 分销
type FenxiaoWithdrawController struct {
	*ShopBaseController
}

func NewFenxiaoWithdrawController(inputReq *BaseReq.I) *FenxiaoWithdrawController {
	return &FenxiaoWithdrawController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *FenxiaoWithdrawController) Page(r *ghttp.Request) *response.JsonResponse {
	if err := r.Parse(&c.Req.PageInfo); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return FenxiaoWithdrawService.Page(r, c.Req)
}

func (c *FenxiaoWithdrawController) ChangeHandle(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	c.Req.AdminId = token.GetLoginMemberId(r)
	return FenxiaoWithdrawService.ChangeHandle(r, c.Req)
}
