package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/FenxiaoUserInvoiceService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

// FenxiaoUserInvoiceController 分销
type FenxiaoUserInvoiceController struct {
	*ShopBaseController
}

func NewFenxiaoUserInvoiceController(inputReq *BaseReq.I) *FenxiaoUserInvoiceController {
	return &FenxiaoUserInvoiceController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *FenxiaoUserInvoiceController) Page(r *ghttp.Request) *response.JsonResponse {
	if err := r.Parse(&c.Req.PageInfo); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return FenxiaoUserInvoiceService.Page(r, c.Req)
}
