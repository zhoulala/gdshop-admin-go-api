package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/FenxiaoUserService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

// FenxiaoUserController 分销
type FenxiaoUserController struct {
	*ShopBaseController
}

func NewFenxiaoUserController(inputReq *BaseReq.I) *FenxiaoUserController {
	return &FenxiaoUserController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *FenxiaoUserController) Page(r *ghttp.Request) *response.JsonResponse {
	if err := r.Parse(&c.Req.PageInfo); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return FenxiaoUserService.Page(r, c.Req)
}

func (c *FenxiaoUserController) SetStatus(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkIds(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return FenxiaoUserService.SetStatus(r, c.Req)
}
