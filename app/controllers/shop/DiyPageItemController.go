package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
)

// DiyPageItemController 装修内容项
type DiyPageItemController struct {
	*ShopBaseController
}

func NewDiyPageItemController(inputReq *BaseReq.I) *DiyPageItemController {
	return &DiyPageItemController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}
