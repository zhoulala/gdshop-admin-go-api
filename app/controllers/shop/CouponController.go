package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/CouponService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

// CouponController 优惠券
type CouponController struct {
	*ShopBaseController
}

func NewCouponController(inputReq *BaseReq.I) *CouponController {
	return &CouponController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *CouponController) Update(r *ghttp.Request) *response.JsonResponse {
	if c.Req.UpdateReq != nil {
		if err := r.Parse(c.Req.UpdateReq); err != nil {
			return response.FailByRequestMessage(r, err.Error())
		}
	}
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return CouponService.Update(r, c.Req)
}

func (c *CouponController) ChangeStatus(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return CouponService.ChangeStatus(r, c.Req)
}

func (c *CouponController) Ending(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return CouponService.Ending(r, c.Req)
}

func (c *CouponController) GetRemark(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return CouponService.GetRemark(r, c.Req)
}

func (c *CouponController) SetRemark(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return CouponService.SetRemark(r, c.Req)
}
