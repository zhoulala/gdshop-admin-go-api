package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/FenxiaoGoodsLevelService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

// FenxiaoGoodsLevelController 分销
type FenxiaoGoodsLevelController struct {
	*ShopBaseController
}

func NewFenxiaoGoodsLevelController(inputReq *BaseReq.I) *FenxiaoGoodsLevelController {
	return &FenxiaoGoodsLevelController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *FenxiaoGoodsLevelController) Info(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return FenxiaoGoodsLevelService.Info(r, c.Req)
}

func (c *FenxiaoGoodsLevelController) Update(r *ghttp.Request) *response.JsonResponse {
	if c.Req.UpdateReq != nil {
		if err := r.Parse(c.Req.UpdateReq); err != nil {
			return response.FailByRequestMessage(r, err.Error())
		}
	}
	/*if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}*/
	return FenxiaoGoodsLevelService.Update(r, c.Req)
}
