package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/service/OrderRefundService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

// OrderRefundController 快递信息
type OrderRefundController struct {
	*ShopBaseController
}

func NewOrderRefundController(inputReq *BaseReq.I) *OrderRefundController {
	inputReq.TableName = "order_refund"
	return &OrderRefundController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *OrderRefundController) GetListTypes(r *ghttp.Request) *response.JsonResponse {
	resMap := g.List{}
	resMap = append(resMap, g.Map{
		"label":   "全部",
		"value":   0,
		"checked": false,
	})
	resMap = append(resMap, g.Map{
		"label":   "待处理",
		"value":   1,
		"checked": true,
	})
	resMap = append(resMap, g.Map{
		"label":   "已驳回",
		"value":   2,
		"checked": false,
	})
	resMap = append(resMap, g.Map{
		"label":   "已同意待退款",
		"value":   3,
		"checked": false,
	})
	resMap = append(resMap, g.Map{
		"label":   "完成已退款",
		"value":   99,
		"checked": false,
	})
	return response.SuccessByRequestMessageData(nil, "获取成功",
		resMap)
}

func (c *OrderRefundController) Page(r *ghttp.Request) *response.JsonResponse {
	if err := r.Parse(&c.Req.PageInfo); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return OrderRefundService.Page(r, c.Req)
}

func (c *OrderRefundController) SubmitHandle(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return OrderRefundService.SubmitHandle(r, c.Req)
}

func (c *OrderRefundController) GetHandle(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return OrderRefundService.GetHandle(r, c.Req)
}

func (c *OrderRefundController) Refund(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return OrderRefundService.Refund(r, c.Req)
}
