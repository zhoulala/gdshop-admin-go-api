package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/request/GoodsReq"
	"gdshop-admin-go-api/app/service/GoodsService"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

// GoodsController 商品
type GoodsController struct {
	*ShopBaseController
}

func NewGoodsController(inputReq *BaseReq.I) *GoodsController {
	return &GoodsController{
		&ShopBaseController{&controllers.BaseController{Req: inputReq}},
	}
}

func (c *GoodsController) Page(r *ghttp.Request) *response.JsonResponse {
	if err := r.Parse(&c.Req.PageInfo); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return GoodsService.Page(r, c.Req)
}

func (c *GoodsController) GetFormPrice(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return GoodsService.GetFormPrice(r, c.Req)
}

func (c *GoodsController) GetFormStock(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return GoodsService.GetFormStock(r, c.Req)
}

func (c *GoodsController) SetFormPrice(r *ghttp.Request) *response.JsonResponse {
	return GoodsService.SetFormPrice(r, c.Req)
}

func (c *GoodsController) SetFormStock(r *ghttp.Request) *response.JsonResponse {
	return GoodsService.SetFormStock(r, c.Req)
}

func (c *GoodsController) Info(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return GoodsService.Info(r.GetCtx(), c.Req)
}

func (c *GoodsController) Add(r *ghttp.Request) *response.JsonResponse {
	var parames *GoodsReq.Update
	if err := r.Parse(&parames); err != nil {
		return response.FailByRequestMessage(r, "Parse "+err.Error())
	}
	c.Req.Id = "0"
	return GoodsService.SaveGoods(r, c.Req, parames)
}

func (c *GoodsController) Update(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkId(r); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	var parames *GoodsReq.Update
	if err := r.Parse(&parames); err != nil {
		return response.FailByRequestMessage(r, "Parse "+err.Error())
	}
	return GoodsService.SaveGoods(r, c.Req, parames)
}

func (c *GoodsController) SetStatus(r *ghttp.Request) *response.JsonResponse {
	if err := c.checkIds(r); err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return GoodsService.SetStatus(r, c.Req)
}

func (c *GoodsController) GetDiyGoodsData(r *ghttp.Request) *response.JsonResponse {
	return GoodsService.GetDiyGoodsData(r, c.Req)
}
