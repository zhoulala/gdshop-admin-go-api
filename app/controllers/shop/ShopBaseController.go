package shop

import (
	"gdshop-admin-go-api/app/controllers"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gvalid"
)

type ShopBaseController struct {
	*controllers.BaseController
}

func (c *ShopBaseController) checkId(r *ghttp.Request) error {
	c.Req.Id = r.GetString("id", "")
	if err := gvalid.CheckValue(r.GetCtx(), c.Req.Id, "required", "ID不能为空"); err != nil {
		return err
	}

	return nil
}
func (c *ShopBaseController) checkIds(r *ghttp.Request) error {
	c.Req.Ids = r.GetArray("ids")
	return nil
}
