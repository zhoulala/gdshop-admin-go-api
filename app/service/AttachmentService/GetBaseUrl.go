package AttachmentService

import (
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

// GetBaseUrl 封面图获取地址前缀
func GetBaseUrl(r *ghttp.Request) *response.JsonResponse {
	return response.SuccessByRequestMessageData(nil, "成功", g.Map{
		"base_url": g.Cfg().GetString("upload.PhotoPreFix"),
	})
}
