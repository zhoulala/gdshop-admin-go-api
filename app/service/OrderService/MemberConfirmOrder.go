package OrderService

import (
	"context"
	"gdshop-admin-go-api/app/request/OrderReq"
	"gdshop-admin-go-api/library/response"
)

// MemberConfirmOrder 会员确认订单
func MemberConfirmOrder(ctx context.Context, p *OrderReq.MemberConfirmOrder) *response.JsonResponse {
	orderModel := getOrderModel(ctx, p.OrderId)
	if orderModel == nil {
		return response.FailByRequestMessage(nil, "找不到订单")
	}
	if orderModel.MemberId != p.MemberId {
		return response.FailByRequestMessage(nil, "不允许操作其他人的订单")
	}
	if orderModel.Status != 4 {
		return response.FailByRequestMessage(nil, "订单当前不允许确认订单")
	}

	return ChangeStatus(ctx, &OrderReq.ChangeStatus{
		OrderId:  p.OrderId,
		Type:     "confirm_order",
		MemberId: p.MemberId,
	})
}
