package OrderService

import (
	"context"
	"gdshop-admin-go-api/app/request/OrderReq"
	"gdshop-admin-go-api/library/response"
)

// 会员关闭付订单
func MemberCloseOrder(ctx context.Context, p *OrderReq.MemberCloseOrder) *response.JsonResponse {
	orderModel := getOrderModel(ctx, p.OrderId)
	if orderModel == nil {
		return response.FailByRequestMessage(nil, "找不到订单")
	}

	if orderModel.Status != 1 && orderModel.Status != 2 {
		// 当前状态已经不允许关闭，照样返回成功，删除任务
		return response.FailByRequestMessage(nil, "订单当前不允许关闭")
	}

	return ChangeStatus(ctx, &OrderReq.ChangeStatus{
		OrderId:  p.OrderId,
		Type:     "close_order",
		MemberId: p.MemberId,
		Remark:   p.Remark,
	})
}
