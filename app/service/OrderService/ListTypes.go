package OrderService

import (
	"context"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/frame/g"
)

func ListTypes(ctx context.Context, req *BaseReq.I) *response.JsonResponse {
	resMap := g.List{}
	resMap = append(resMap, g.Map{
		"label":   "全部",
		"value":   0,
		"checked": false,
	})
	resMap = append(resMap, g.Map{
		"label":   "待付款",
		"value":   1,
		"checked": true,
	})
	resMap = append(resMap, g.Map{
		"label":   "待备货",
		"value":   2,
		"checked": false,
	})
	resMap = append(resMap, g.Map{
		"label":   "待发货",
		"value":   3,
		"checked": false,
	})
	resMap = append(resMap, g.Map{
		"label":   "待收货",
		"value":   4,
		"checked": false,
	})
	resMap = append(resMap, g.Map{
		"label":   "待评价",
		"value":   5,
		"checked": false,
	})
	resMap = append(resMap, g.Map{
		"label":   "已完成",
		"value":   99,
		"checked": false,
	})
	resMap = append(resMap, g.Map{
		"label":   "已取消",
		"value":   6,
		"checked": false,
	})
	return response.SuccessByRequestMessageData(nil, "获取成功",
		resMap)
}
