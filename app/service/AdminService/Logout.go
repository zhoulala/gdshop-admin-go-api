package AdminService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	"gdshop-admin-go-api/library/tools/cacheHelp"
	"gdshop-admin-go-api/library/tools/jwt"
	"gdshop-admin-go-api/library/tools/token"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
)

func Logout(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	// 清除缓存
	cacheHelp.ClaerAdminCaches(gconv.String(req.AdminId))
	jwt.Layout(req.AdminId, token.GetToken(r))
	return response.SuccessByRequestMessage(nil, "登出成功")
}
