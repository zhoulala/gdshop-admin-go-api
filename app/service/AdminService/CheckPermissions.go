package AdminService

import (
	"gdshop-admin-go-api/library/tools/token"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"strings"
)

// CheckPermissions 检测权限，第一期不开放
func CheckPermissions(r *ghttp.Request) bool {
	memberId := token.GetLoginMemberId(r)
	if memberId < 1 {
		return false
	}
	isAdmin := false
	// 根据登录人ID，获取到权限
	v, err := g.Redis().DoVar("HGET", "IsAdmin",
		memberId)
	if err == nil {
		isAdmin = v.Bool()
	}

	perms := strings.Replace(strings.Trim(r.Request.URL.Path, "/"), "/", ":", -1)
	//fmt.Println("isAdmin " , isAdmin)
	//fmt.Println("perms " , perms)
	return CheckPermsByAdminId(r.Context(), perms, memberId, isAdmin)
}
