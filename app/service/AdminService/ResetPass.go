package AdminService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	"gdshop-admin-go-api/library/tools"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/grand"
	"time"
)

func ResetPass(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	passwordSalt := grand.Letters(10)
	defaultPassword := "123456"
	_, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "admin").Where("id IN (?)", req.Ids).Update(g.Map{
		"password_salt": passwordSalt,
		"password":      tools.EncryptPassword(defaultPassword, passwordSalt),
		"updated_at":    time.Now().Unix(),
	})
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return response.SuccessByRequestMessageData(nil, "新密码："+defaultPassword, g.Map{
		"id": req.Ids,
	})
}
