package ArticleService

import (
	"context"
	"database/sql"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
)

func Info(ctx context.Context, req *BaseReq.I) *response.JsonResponse {
	obj := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, req.TableName).WherePri(
		"id",
		req.Id,
	)
	// 过滤字段
	if len(req.InfoIgnoreProperty) > 0 {
		obj.Fields(obj.GetFieldsExStr(req.InfoIgnoreProperty))
	}
	res, err := obj.One()
	if err != nil {
		if err != sql.ErrNoRows {
			// 非 结果为空
			return response.FailByRequestMessage(nil, err.Error())
		}
	}
	result := res.Map()
	resCon, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "article_content").Where(
		"article_id",
		result["id"],
	).Fields("content").Value()
	result["content"] = resCon.String()
	return response.SuccessByRequestMessageData(nil, "获取成功",
		result)
}
