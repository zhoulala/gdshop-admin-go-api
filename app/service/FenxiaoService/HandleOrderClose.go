package FenxiaoService

import (
	"context"
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/frame/g"
)

// HandleOrderClose 处理订单关闭
func HandleOrderClose(orderId int) {
	ctx := context.Background()
	err := g.DB().Transaction(ctx, func(ctx context.Context, tx *gdb.TX) error {
		_, err := tx.Model("fenxiao_order").Unscoped().Where(
			"order_id = ?", orderId).Update(g.Map{
			"status": 10,
		})
		if err != nil {
			return err
		}
		_, err = tx.Model("fenxiao_order_details").Unscoped().Where(
			"order_id = ?", orderId).Update(g.Map{
			"status": 10,
		})
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return
	}

}
