package FenxiaoService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
)

func GetLevel(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	value, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "fenxiao_level").
		Where("level_weight=0").Fields("one_rate,two_rate,three_rate").One()
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}

	if value.IsEmpty() {
		return response.SuccessByRequestMessageData(r, "成功", g.Map{})
	}
	return response.SuccessByRequestMessageData(r, "成功", g.Map{
		"one_rate":   gconv.Float64(value["one_rate"]) / 1000,
		"two_rate":   gconv.Float64(value["two_rate"]) / 1000,
		"three_rate": gconv.Float64(value["three_rate"]) / 1000,
	})
}
