package FenxiaoService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
)

func ChangeStatus(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	_, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, req.TableName).Where(
		"id",
		gconv.SliceAny(req.Ids),
	).Update(g.Map{
		"status": gdb.Raw("ABS(status-1)"),
	})
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return response.SuccessByRequestMessage(nil, "切换成功")
}
