package FenxiaoService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func SetLevel(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	_, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "fenxiao_level").Where("level_weight=0").Update(g.Map{
		"one_rate":   r.GetFloat64("one_rate") * 1000,
		"two_rate":   r.GetFloat64("two_rate") * 1000,
		"three_rate": r.GetFloat64("three_rate") * 1000,
	})
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return response.SuccessByRequestMessage(r, "设置成功")
}
