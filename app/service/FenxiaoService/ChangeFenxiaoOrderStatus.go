package FenxiaoService

import (
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/frame/g"
)

func ChangeFenxiaoOrderStatus(tx *gdb.TX, orderId, beforeStatus, status int) error {
	where := "order_id = ?"
	if beforeStatus > 0 {
		where += " AND status = ?"
	}

	_, err := tx.Model("fenxiao_order").Unscoped().Where(
		where, beforeStatus, orderId).Update(g.Map{
		"status": status,
	})
	if err != nil {
		return err
	}
	_, err = tx.Model("fenxiao_order_details").Unscoped().Where(
		"status = ? AND order_id = ?", beforeStatus, orderId).Update(g.Map{
		"status": status,
	})
	if err != nil {
		return err
	}

	return nil
}
