package FenxiaoService

import (
	"fmt"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	"gdshop-admin-go-api/library/tools"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
)

func Surveys(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	fanhui := g.Map{}
	withdrawal := []string{}
	// 可提现佣金（元）
	sum, err := toolsDb.GetUnSafaTable(ctx, "fenxiao_order_details").
		Where("delete_at<1 AND status = 90").Sum("paid*rate/10000000")
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	withdrawal = append(withdrawal, fmt.Sprintf("%.2f", sum))
	// 提现待审核（元）
	sum, err = toolsDb.GetUnSafaTable(ctx, "fenxiao_withdraw").
		Where("delete_at<1 AND status = 1").Sum("amount_received / 100")
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	withdrawal = append(withdrawal, fmt.Sprintf("%.2f", sum))
	// 提现成功佣金（元）
	sum, err = toolsDb.GetUnSafaTable(ctx, "fenxiao_withdraw").
		Where("delete_at<1 AND status = 99").Sum("amount_received / 100")
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	withdrawal = append(withdrawal, fmt.Sprintf("%.2f", sum))
	// 进行中佣金（元）
	sum, err = toolsDb.GetUnSafaTable(ctx, "fenxiao_order_details").
		Where("delete_at<1 AND status = 1").Sum("paid*rate/10000000")
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	withdrawal = append(withdrawal, fmt.Sprintf("%.2f", sum))

	user := []string{}
	count, err := toolsDb.GetUnSafaTable(ctx, "fenxiao_user").
		Where("delete_at<1 AND is_verify=0").
		Fields("1").Count()
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	user = append(user, gconv.String(count))
	count, err = toolsDb.GetUnSafaTable(ctx, "fenxiao_user").
		Where("delete_at<1").
		Fields("1").Count()
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	user = append(user, gconv.String(count))

	total := []string{}
	// 分销订单总额（元）
	sum, err = toolsDb.GetUnSafaTable(ctx, "fenxiao_order").
		Where("delete_at<1 AND status IN (1,90,99)").Sum("paid / 100")
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	total = append(total, gconv.String(sum))
	// 分销佣金总额（元）
	sum, err = toolsDb.GetUnSafaTable(ctx, "fenxiao_order_details").
		Where("delete_at<1 AND status IN (1,90,99)").Sum("paid*rate/10000000")
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	total = append(total, gconv.String(tools.Decimal(sum)))
	// 分销商品数
	count, err = toolsDb.GetUnSafaTable(ctx, "fenxiao_goods").As("fg").LeftJoin("goods g", "fg.goods_id = g.id").
		Where("fg.delete_at<1 AND g.delete_at<1").
		Fields("1").Count()
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	total = append(total, gconv.String(count))

	fanhui["withdrawal"] = withdrawal
	fanhui["user"] = user
	fanhui["total"] = total
	return response.SuccessByRequestMessageData(r, "成功", fanhui)
}
