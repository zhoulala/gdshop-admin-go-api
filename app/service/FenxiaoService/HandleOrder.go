package FenxiaoService

import (
	"context"
	"errors"
	"gdshop-admin-go-api/app/entity"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/frame/g"
	"time"
)

// HandleOrderByOrderId 根据订单ID 处理分销订单，一般由支付完成事件触发
func HandleOrderByOrderId(orderId int) {
	ctx := context.Background()
	var foList []*entity.FenxiaoOrder
	err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "fenxiao_order").Where(
		"handle_status=0 AND order_id=?", orderId).Scan(&foList)
	if err != nil {
		return
	}

	if foList == nil {
		return
	}
	err = g.DB().Transaction(ctx, func(ctx context.Context, tx *gdb.TX) error {
		for _, item := range foList {
			err := HandleOrderByFenxiaoOrder(ctx, tx, item)
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		return
	}
}

// HandleOrderByFenxiaoOrder 根据分销订单ID 处理信息
func HandleOrderByFenxiaoOrder(ctx context.Context, tx *gdb.TX, fo *entity.FenxiaoOrder) error {
	var osModel *entity.OrderSub
	err := tx.Model("order_sub").Where("id", fo.OrderSubId).Scan(&osModel)
	if err != nil {
		return err
	}
	if osModel == nil {
		return errors.New("HandleOrderByFenxiaoOrder 找不到子订单数据")
	}
	foData := g.Map{
		"paid": osModel.Paid,
	}
	// 开启自购分佣
	if fo.OneFenxiaoUserId > 0 {
		one := GetUserInfoAndGoodsRateByFenxiaoUserId(ctx, fo.OneFenxiaoUserId, 1, osModel.GoodsId)
		if one != nil {
			foData["one_rate"] = one.Rate * 100000.00
			foData["one_level_id"] = one.LevelId
			foData["one_fenxiao_user_id"] = one.FenxiaoUserId
			foData["one_member_id"] = one.MemberId
			_, err := tx.Model("fenxiao_order_details").InsertAndGetId(g.Map{
				"fenxiao_order_id": fo.Id,
				"order_id":         fo.OrderId,
				"order_sub_id":     fo.OrderSubId,
				"paid":             osModel.Paid,
				"layer":            1,
				"rate":             foData["one_rate"],
				"level_id":         one.LevelId,
				"fenxiao_user_id":  one.FenxiaoUserId,
				"member_id":        one.MemberId,
				"status":           one.Status,
				"create_at":        time.Now().Unix(),
			})
			if err != nil {
				return err
			}
		}
	}
	// 上级
	if fo.TwoFenxiaoUserId > 0 {
		two := GetUserInfoAndGoodsRateByFenxiaoUserId(ctx, fo.TwoFenxiaoUserId, 2, osModel.GoodsId)
		if two != nil {
			foData["two_rate"] = two.Rate * 100000.00
			foData["two_level_id"] = two.LevelId
			foData["two_fenxiao_user_id"] = two.FenxiaoUserId
			foData["two_member_id"] = two.MemberId
			_, err := tx.Model("fenxiao_order_details").InsertAndGetId(g.Map{
				"fenxiao_order_id": fo.Id,
				"order_id":         fo.OrderId,
				"order_sub_id":     fo.OrderSubId,
				"paid":             osModel.Paid,
				"layer":            2,
				"rate":             foData["two_rate"],
				"level_id":         two.LevelId,
				"fenxiao_user_id":  two.FenxiaoUserId,
				"member_id":        two.MemberId,
				"status":           two.Status,
				"create_at":        time.Now().Unix(),
			})
			if err != nil {
				return err
			}
		}
	}
	// 上上级
	if fo.ThreeFenxiaoUserId > 0 {
		three := GetUserInfoAndGoodsRateByFenxiaoUserId(ctx, fo.ThreeFenxiaoUserId, 3, osModel.GoodsId)
		if three != nil {
			foData["three_rate"] = three.Rate * 100000.00
			foData["three_level_id"] = three.LevelId
			foData["three_fenxiao_user_id"] = three.FenxiaoUserId
			foData["three_member_id"] = three.MemberId
			_, err := tx.Model("fenxiao_order_details").InsertAndGetId(g.Map{
				"fenxiao_order_id": fo.Id,
				"order_id":         fo.OrderId,
				"order_sub_id":     fo.OrderSubId,
				"paid":             osModel.Paid,
				"layer":            3,
				"rate":             foData["three_rate"],
				"level_id":         three.LevelId,
				"fenxiao_user_id":  three.FenxiaoUserId,
				"member_id":        three.MemberId,
				"status":           three.Status,
				"create_at":        time.Now().Unix(),
			})
			if err != nil {
				return err
			}
		}
	}

	foData["update_at"] = time.Now().Unix()
	foData["handle_status"] = 1

	_, err = tx.Model("fenxiao_order").Where("id", fo.Id).Update(foData)
	if err != nil {
		return err
	}

	return nil
}
