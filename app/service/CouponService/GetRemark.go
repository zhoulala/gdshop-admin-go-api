package CouponService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/net/ghttp"
)

func GetRemark(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	v, err := toolsDb.GetUnSafaTable(ctx, "coupon_remark").Where("coupon_id", req.Id).
		Value("remark")
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}

	return response.SuccessByRequestMessageData(nil, "成功", v.String())
}
