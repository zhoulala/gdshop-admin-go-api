package CouponService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func SetRemark(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	update, err := toolsDb.GetUnSafaTable(ctx, "coupon_remark").Where(
		"coupon_id = ? AND status < 3", req.Id).Save(g.Map{
		"coupon_id": req.Id,
		"remark":    r.Get("remark"),
	})
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	affected, err := update.RowsAffected()
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	if affected < 1 {
		return response.FailByRequestMessage(r, "失败")
	}
	return response.SuccessByRequestMessageData(nil, "成功", g.Map{})
}
