package CouponService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"time"
)

func ChangeStatus(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	update, err := toolsDb.GetUnSafaTable(ctx, req.TableName).Where("id = ? AND status < 3", req.Id).Update(g.Map{
		"status":    gdb.Raw("(CASE WHEN status = 2 THEN 1 ELSE 2 END)"),
		"update_at": time.Now().Unix(),
	})
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	affected, err := update.RowsAffected()
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	if affected < 1 {
		return response.FailByRequestMessage(r, "失败")
	}
	return response.SuccessByRequestMessageData(nil, "成功", g.Map{})
}
