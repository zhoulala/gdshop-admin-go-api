package GoodsActivityService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/net/ghttp"
)

func GetExcludeGoodsSelects(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	all, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "goods").Fields("id,goods_name").Where(
		"id IN (?)",
		req.Ids,
	).All()
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return response.SuccessByRequestMessageData(nil, "成功", all)
}
