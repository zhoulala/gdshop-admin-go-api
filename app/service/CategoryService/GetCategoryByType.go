package CategoryService

import (
	"context"
	"gdshop-admin-go-api/library/tools"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/util/gconv"
)

// GetCategoryByType 按分类字符串获取分类列表
func GetCategoryByType(ctx context.Context, typeStr string) (g.List, error) {
	res, err := toolsDb.GetUnSafaTable(ctx, "category").Where("type IN (?)",
		tools.CategoryTypeStrToTypeIds(typeStr)).All()
	if err != nil {
		return nil, err
	}
	return res.List(), nil
}

// GetCategoryArrayType 按分类字符串获取分类列表
func GetCategoryArrayType(ctx context.Context, typeStr string) (g.Map, error) {
	res, err := toolsDb.GetUnSafaTable(ctx, "category").Where("type IN (?)",
		tools.CategoryTypeStrToTypeIds(typeStr)).Fields("id,cat_name").All()
	if err != nil {
		return nil, err
	}
	resArr := g.Map{}
	for _, item := range res.List() {
		resArr[gconv.String(item["id"])] = item["cat_name"]
	}
	return resArr, nil
}
