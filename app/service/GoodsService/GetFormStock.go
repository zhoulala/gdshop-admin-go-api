package GoodsService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
)

func GetFormStock(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	return GetQuickEditForm(r.GetCtx(), "stock", req.Id)
}
