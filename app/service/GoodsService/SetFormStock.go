package GoodsService

import (
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/app/request/GoodsReq"
	"gdshop-admin-go-api/library/response"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
)

func SetFormStock(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	var parames *GoodsReq.SetFormStock
	if err := r.Parse(&parames); err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}
	return SetQuickEditForm(r.GetCtx(), "stock", parames.GoodsId, gconv.Maps(parames.OptionData))
}
