package FenxiaoUserService

import (
	"context"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	"gdshop-admin-go-api/library/tools"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func Page(r *ghttp.Request, req *BaseReq.I) *response.JsonResponse {
	ctx := r.GetCtx()
	obj := toolsDb.GetUnSafaTable(ctx, "fenxiao_user").As("fu").LeftJoin("member m", "m.id = fu.member_id").
		LeftJoin("fenxiao_user fun1", "fun1.id = fu.parent_id").
		Order("fu.id DESC")
	countObj := toolsDb.GetUnSafaTable(ctx, "fenxiao_user").As("fu").LeftJoin("member m", "m.id = fu.member_id").
		LeftJoin("fenxiao_user fun1", "fun1.id = fu.parent_id")
	SelectFields := g.ArrayStr{
		"fu.*",
		"m.realname AS m_realname",
		"fun1.name AS fun1_name",
		"(SELECT SUM((fod.paid * fod.rate / 100000)) FROM `gd_fenxiao_order_details` fod WHERE (fod.delete_at < 1) AND (fod.status IN (1,90,99) AND fod.fenxiao_user_id = fu.id)) AS brokerage",
		"(SELECT SUM(fw.amount_received) FROM `gd_fenxiao_withdraw` fw WHERE (fw.delete_at < 1) AND (fw.status = 99 AND fw.fenxiao_user_id = fu.id)) AS amount_received",
		"( SELECT COUNT(1) FROM `gd_fenxiao_user` fu1 WHERE (fu1.delete_at < 1) AND (fu1.status = 1 AND (fu1.parent_id = fu.id OR fu1.parent_parent_id = fu.id)) ) AS team_num",
		//"( SELECT COUNT(1) FROM `gd_fenxiao_user` fu1 WHERE (fu1.delete_at < 1) AND (fu1.status = 1 AND (fu1.parent_id = fu.id)) ) AS team_num",
		//"( SELECT COUNT( 1 ) FROM `gd_fenxiao_member_user_map` mum WHERE mum.fenxiao_user_id = fu.id ) AS mum_num",
		// 团队人数应该不包含推荐的会员
		"( 0 ) AS mum_num",
	}

	obj.Where("fu.delete_at<1")
	countObj.Where("fu.delete_at<1")

	countObj.Fields("1")
	obj.Fields(SelectFields)

	keyWord := r.GetString("keyWord")
	status := r.GetString("status")
	whereMap := g.Map{}
	if len(keyWord) > 0 {
		whereMap["m.realname LIKE ? OR fu.name LIKE ?"] = []string{
			"%" + keyWord + "%", "%" + keyWord + "%",
		}
	}
	if len(status) > 0 {
		whereMap["fu.status"] = status
	}
	countObj.Where(whereMap)
	obj.Where(whereMap)

	count, err := countObj.Count()
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}

	res, err := obj.Page(req.Page, req.PageSize).All()
	if err != nil {
		return response.FailByRequestMessage(r, err.Error())
	}

	resList := res.List()

	/*for _, item := range resList {

	}*/

	return response.SuccessByRequestMessageData(nil, "获取成功",
		g.Map{
			"list":       resList,
			"pagination": tools.GetPageInfo(count, req.Page, req.PageSize),
		})
}

// 已经获取的佣金
func GetBrokerageByFenxiaoUserId(ctx context.Context, fenxiaoUserId int) float64 {
	sum, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "fenxiao_order_details").Where(
		"status IN (1,90,99) AND fenxiao_user_id = ?", fenxiaoUserId).Sum("(paid * rate / 100000)")
	if err != nil {
		return 0
	}

	return sum
}

// 已提现的佣金
func GetAmountReceivedByFenxiaoUserId(ctx context.Context, fenxiaoUserId int) float64 {
	sum, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "fenxiao_withdraw").Where(
		"status = 99 AND fenxiao_user_id = ?", fenxiaoUserId).Sum("amount_received")
	if err != nil {
		return 0
	}

	return sum
}
