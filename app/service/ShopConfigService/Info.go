package ShopConfigService

import (
	"context"
	"gdshop-admin-go-api/app/request/BaseReq"
	"gdshop-admin-go-api/library/response"
	toolsDb "gdshop-admin-go-api/library/tools/db"
	"github.com/gogf/gf/encoding/gjson"
	"github.com/gogf/gf/frame/g"
)

func Info(ctx context.Context, req *BaseReq.I) *response.JsonResponse {
	configId := "shop_config"
	value, err := toolsDb.GetUnSafaTableAddDeleteWhere(ctx, "sys_config").Where("config_key", configId).Value("config_value")
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}

	if value.IsEmpty() {
		return response.SuccessByRequestMessageData(nil, "成功", g.Map{})
	}
	json, err := gjson.LoadContent(value)
	if err != nil {
		return response.FailByRequestMessage(nil, err.Error())
	}
	return response.SuccessByRequestMessageData(nil, "成功", json)
}
