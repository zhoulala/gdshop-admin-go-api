package banner

import (
	"fmt"
	"github.com/gogf/gf"
	"github.com/gogf/gf/frame/g"
	"io/ioutil"
	"os"
	"runtime"
	"time"
)

type bannerText struct {
}

func (t *bannerText) Env(env string) string {
	return os.Getenv(env)
}
func (v *bannerText) Now(layout string) string {
	return time.Now().Format(layout)
}

// 初始化启动banner
func InitBanner() {
	bBanner, err := ioutil.ReadFile("config/banner.txt")
	if err == nil {
		//banner.Init(os.Stdout,true,true,bytes.NewBufferString(string(bBanner)))
		gfView := g.View()
		t := &bannerText{}
		casnshu := g.Map{
			"t":              t,
			"GoVersion":      runtime.Version(),
			"GoFrameVersion": gf.VERSION,
			"GdbaseVersion":  g.Cfg().GetString("site.GdbaseVersion"),
			"GOOS":           runtime.GOOS,
			"GOARCH":         runtime.GOARCH,
			"NumCPU":         runtime.NumCPU(),
			"GOPATH":         os.Getenv("GOPATH"),
			"GOROOT":         runtime.GOROOT(),
			"Compiler":       runtime.Compiler,
		}
		r, err := gfView.ParseContent(nil, string(bBanner), casnshu)
		if err != nil {
			g.Dump(err.Error())
		}
		fmt.Println(r)
	}
}
