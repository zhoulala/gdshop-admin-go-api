package HttpQueue

import "github.com/gogf/gf/frame/g"

// AddCompleteOrder 发货成功后 15 天自动完成订单，转成待评价
func AddCompleteOrder(orderId int) {
	g.Client().GetContent(g.Cfg().GetString("server.Url")+
		"/gdshop/queue/add_complete_order", g.Map{
		"order_id": orderId,
	})
}
