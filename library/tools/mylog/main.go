package mylog

import "github.com/gogf/gf/frame/g"

func InfoLog(v ...interface{}) {
	g.Log().Async().Info(v)
}

func ErrorLog(v ...interface{}) {
	g.Log().Async().Error(v)
}
