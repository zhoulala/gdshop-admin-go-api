package genids

import (
	"github.com/gogf/gf/frame/g"
)

func ToolsGetNextId() (int64, error) {
	idGen := GetIdWokrer(g.Cfg().GetInt64("site.NodeID"))
	return idGen.GetNextId()
}
