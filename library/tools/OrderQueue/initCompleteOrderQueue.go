package OrderQueue

import (
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/os/gtimer"
	"time"
)

// 发货成功后 15 天自动完成订单， 300秒执行一次
func initCompleteOrderQueue() {
	topicName := "CompleteOrder"
	interval := 3 * 1400 * time.Millisecond
	qcli := Client{
		Conn: g.Redis("queue").Conn(),
	}

	gtimer.Add(interval, func() {
		msg, isNil, err := qcli.Pop(topicName)
		if isNil {
			// 无任务
			return
		}
		if err != nil {
			g.Log().Async().Cat("OrderQueue").Error(topicName, " 提取失败:", err.Error())
			return
		}
		g.Log().Async().Cat("OrderQueue").Info(topicName, " 开始消费 :", msg)
		handleOrderQueue(msg)
	})

	// 一天扫一次数据库，完成发货超过 15 天的订单
}
