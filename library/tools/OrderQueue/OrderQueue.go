package OrderQueue

// OrderQueue 队列内容
type OrderQueue struct {
	OrderId int // 订单ID
	Type    int // 类型 0=关闭订单 其他的后续加
	ReTry   int // 重试次数
}
